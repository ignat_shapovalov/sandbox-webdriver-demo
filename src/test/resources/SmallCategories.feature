@ST_29507 @Feature_SmallCategories
Feature: SmallCategories

@SC_118475 @High @Slider @Small
Scenario: Slider
Given I'm on main page
And I select "Slider" category
And Drag slider

@SC_118476 @High @Small @Tabs
Scenario: Tabs
Given I'm on main page
And I select "Tabs" category
And Select tabs

@SC_118477 @High @Small @Tooltip
Scenario: Tooltip
Given I'm on main page
And I select "Tooltip" category
And I click on "Default functionality" button
And I navigate to 'Your age' textinput
And I click on "custom animation demo" button
And I show all custom animation