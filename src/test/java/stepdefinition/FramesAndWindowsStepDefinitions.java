package stepdefinition;

import arp.CucumberArpReport;
import arp.ReportService;
import cucumber.api.java.en.And;
import org.springframework.beans.factory.annotation.Autowired;
import pages.base.PageInstance;
import pages.categories.FramesAndWindows;

/**
 * Created by pchelintsev on 5/19/2016.
 */
public class FramesAndWindowsStepDefinitions extends PageInstance {

    @Autowired
    private
    FramesAndWindows framesAndWindows;


    @And("^I open new tab$")
    public void iOpenNewTab() throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            framesAndWindows.newBrowserTab.click();
            framesAndWindows.switchToNewWindow();
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I open new window$")
    public void iOpenNewWindow() throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            framesAndWindows.openNewSeprateWindow.click();
            framesAndWindows.switchToNewWindow();
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I open new frame$")
    public void iOpenNewFrame() throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            framesAndWindows.openFramesetWindow.click();
            framesAndWindows.switchToNewWindow();
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }
}
