package pages.categories;

import com.unitedsofthouse.ucucumberpackage.typesfactory.types.DropDown;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Lazy;
import pages.base.BasePage;

/**
 * Created by pchelintsev on 5/19/2016.
 */
public class Slider extends BasePage {
    @Override
    protected WebElement elementForLoading() throws Exception {
        return null;
    }

    /**
     * This method drag slider in different directions
     *
     * @throws InterruptedException
     */
    public void moveSlider() throws InterruptedException {
        Thread.sleep(1000);
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='slider-range-max']/span")), 50, 0).perform();
        Thread.sleep(1000);
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='slider-range-max']/span")), 100, 0).perform();
        Thread.sleep(1000);
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='slider-range-max']/span")), 150, 0).perform();
        Thread.sleep(1000);
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='slider-range-max']/span")), -50, 0).perform();
        Thread.sleep(1000);
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='slider-range-max']/span")), -100, 0).perform();
        Thread.sleep(1000);
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='slider-range-max']/span")), -200, 0).perform();
        Thread.sleep(1000);
        (new Actions(driver)).dragAndDropBy(driver.findElement(By.xpath(".//*[@id='slider-range-max']/span")), 600, 0).perform();

    }
}
